# Employee Management

Mini project for manage Employees

## Version Requirement

<table>
  <tr>
    <th>Technology</th>
    <th>Version</th>
  </tr>
  <tr>
    <td>Node.Js</td>
    <td><code>16.20.2</code></td>
  </tr>
  <tr>
    <td>NPM</td>
    <td><code>8.19.4</code></td>
  </tr>
  <tr>
    <td>Angular CLI</td>
    <td><code>16.2.14</code></td>
  </tr>
</table>

## Install Angular Cli

Run `npm install -g @angular/cli` or install specific version `npm install -g @angular/cli@17`

## Running Project

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

Run specific port `ng serve --port 9200`

### Login User

username: `admin`
password: `admin`


