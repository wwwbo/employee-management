import { Menus } from '../models/app.model';

export const MenuSideNav: Menus[] = [
  { path: '/admin/home', menuName: 'Home', menuIcon: 'home' },
  { path: '/admin/employee', menuName: 'Employee', menuIcon: 'people_outline' },
];
