import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { BreakpointObserver } from '@angular/cdk/layout';
import { Menus } from '../../models/app.model';
import { MenuSideNav } from '../../constants/app-menu';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
})
export class AdminComponent implements OnInit {
  @ViewChild(MatSidenav)
  sidenav!: MatSidenav;
  isMobile: boolean = true;
  isCollapsed: boolean = true;
  isCollapsedMobile: boolean = true;
  sideNavMenus: Menus[] | undefined;
  currentPath: string = '';

  constructor(private _observer: BreakpointObserver, private _router: Router) {}

  ngOnInit(): void {
    this.initData();
    this.onSetScreenSize();
  }

  initData() {
    this.sideNavMenus = MenuSideNav;
  }

  onNavigateMenu(path: string) {
    if (this.isMobile) {
      this.sidenav.toggle();
      this.isCollapsedMobile = !this.isCollapsedMobile;
    }
    this.currentPath = path;
    this._router.navigate([path]);
  }

  onSetScreenSize() {
    this._observer.observe(['(max-width: 800px)']).subscribe((screenSize) => {
      if (screenSize.matches) {
        this.isMobile = true;
      } else {
        this.isMobile = false;
      }
    });
  }

  toggleMenu() {
    if (this.isMobile) {
      this.sidenav.toggle();
      this.isCollapsed = false;
      this.isCollapsedMobile = !this.isCollapsedMobile;
    } else {
      this.sidenav.open();
      this.isCollapsed = !this.isCollapsed;
    }
  }

  getIcon() {
    return this.isMobile
      ? this.isCollapsedMobile
        ? 'menu_open'
        : 'close'
      : this.isCollapsed
      ? 'menu_open'
      : 'close';
  }
}
