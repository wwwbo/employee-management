export interface Menus {
  path: string;
  menuName: string;
  menuIcon: string
}

export interface Response<T> {
  status: boolean;
  statusCode: number;
  data: T
}