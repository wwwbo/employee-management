import { Injectable } from '@angular/core';
import { SnackbarComponent } from '../components/snackbar/snackbar.component';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root',
})
export class SharedService {
  constructor(private _snackBar: MatSnackBar) {}

  onShowSnackBar(snackBarMessage: string, snackBarAction: boolean = false) {
    this._snackBar.openFromComponent(SnackbarComponent, {
      data: snackBarMessage,
      duration: 2 * 1000,
      verticalPosition: 'top',
      horizontalPosition: 'right',
      panelClass: snackBarAction
        ? 'custom-snackbar-delete'
        : 'custom-snackbar-edit',
    });
  }
}
