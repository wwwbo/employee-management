import { Component, OnDestroy, OnInit } from '@angular/core';
import { EmployeeService } from '../../services/employee.service';
import { Employees } from '../../models/employee.model';
import { Subject } from 'rxjs';
import { StateService } from '../../services/state.service';

@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.scss'],
})
export class EmployeeDetailComponent implements OnInit, OnDestroy {
  onDestroy$ = new Subject<void>();
  employees!: Employees;

  constructor(
    private _stateService: StateService,
    private _employeeService: EmployeeService
  ) {}

  ngOnInit(): void {
    this.initData();
  }

  initData() {
    const employeeId = this._stateService.getEmployeeId();
    if (employeeId === 0)
      this._employeeService.onNavigatePage('/admin/employee');
    this.employees = this._employeeService.getEmployeeId(employeeId);
  }

  onCloseDetail() {
    this._employeeService.onNavigatePage('/admin/employee')
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
