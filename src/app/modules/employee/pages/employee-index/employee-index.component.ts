import {
  AfterViewInit,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { StateService } from '../../services/state.service';
import { EmployeeService } from '../../services/employee.service';
import { Subject, firstValueFrom, takeUntil } from 'rxjs';
import { Employees, Employee } from '../../models/employee.model';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-employee-index',
  templateUrl: './employee-index.component.html',
  styleUrls: ['./employee-index.component.scss'],
})
export class EmployeeIndexComponent
  implements OnInit, AfterViewInit, OnDestroy
{
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  onDestroy$ = new Subject<void>();
  employees: Employees | undefined;
  searchVal: string = '';
  displayedColumns: string[] = [
    'firstName',
    'email',
    'status',
    'group',
    'action',
  ];
  dataSource = new MatTableDataSource<Employee>();

  constructor(
    private _stateService: StateService,
    private _employeeService: EmployeeService
  ) {}

  ngOnInit(): void {
    this.initData();
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  async initData() {
    const checkEmployeeData = this._employeeService.getEmployees().length > 0;
    if (!checkEmployeeData)
      await firstValueFrom(this._employeeService.fetchDataEmployee());

    this._employeeService.currentEmployeeData$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((data: Employees) => {
        if (data) {
          this.employees = data;
          this.dataSource.data = data;
        }
      });

    this._employeeService.searchKey$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((data: string) => {
        this.searchVal = data;
        this.dataSource.filter = data;
      });
  }

  onClickDetail(employeeId: number) {
    this._stateService.setEmployeeId(employeeId);
    this._employeeService.onNavigatePage('admin/employee/detail');
  }

  onClickCreateUpdate(employeeId: number = 0) {
    this._stateService.setEmployeeId(employeeId);

    setTimeout(() => {
      if (employeeId !== 0)
        this._employeeService.onNavigatePage('admin/employee/update');
      this._employeeService.onNavigatePage('admin/employee/create');
    }, 200);
  }

  onClickDelete(employeeId: number) {
    this._employeeService.onDeleteEmployee(employeeId);
  }

  onKeyPress() {}

  onFilterData(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    this._employeeService.onSetValueSearch(this.dataSource.filter);

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getEmployeeStatus(status: boolean) {
    return status ? 'Active' : 'Inactive';
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
