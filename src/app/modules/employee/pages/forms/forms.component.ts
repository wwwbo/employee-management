import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { StateService } from '../../services/state.service';
import {
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Employee } from '../../models/employee.model';
import { EmployeeService } from '../../services/employee.service';
import { MatRadioChange } from '@angular/material/radio';

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.scss'],
})
export class FormsComponent implements OnInit, OnDestroy {
  onDestroy$ = new Subject<void>();
  private employeeId: number = 0;
  employeeForms!: FormGroup;
  selectedValue: string = '';
  filterDate = (d: Date | null): boolean => {
    const today = new Date();
    return d ? d < today : false;
  }

  constructor(
    private _stateService: StateService,
    private _employeeService: EmployeeService,
    private _fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.employeeId = this._stateService.getEmployeeId();
    this.initForms();
    if (this.employeeId) this.onSetFormsData();
  }

  initForms() {
    this.employeeForms = this._fb.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      email: ['', [Validators.required]],
      birthDate: ['', [Validators.required]],
      basicSalary: ['', [Validators.required]],
      groupName: ['', [Validators.required]]
    });
  }

  onSetFormsData() {
    const currentData = this._employeeService.getEmployeeId(this.employeeId);
    currentData.forEach((d) => {
      this.employeeForms.setValue({
        firstName: d.firstName,
        lastName: d.lastName,
        email: d.email,
        birthDate: d.birthDate,
        basicSalary: d.basicSalary,
        groupName: d.group,
      });

      this.selectedValue = d.status ? '1' : '0';
    });
  }

  onSubmit() {
    if (!this.employeeForms.valid) return;
    if (this.employeeId > 0) {
      this._employeeService.onUpdateEmployee(this.onMappingData());
    } else {
      this._employeeService.onCreateEmployee(this.onMappingData());
    }
    this._employeeService.onNavigatePage('/admin/employee');
  }

  onMappingData() {
    const employeeId = this.getLastId();
    const data: Employee = {
      employeeId: this.employeeId > 0 ? this.employeeId : employeeId,
      firstName: this.employeeForms.get('firstName')?.value,
      lastName: this.employeeForms.get('lastName')?.value,
      email: this.employeeForms.get('email')?.value,
      birthDate: this.employeeForms.get('birthDate')?.value,
      basicSalary: this.employeeForms.get('basicSalary')?.value,
      group: this.employeeForms.get('groupName')?.value,
      status: this.selectedValue === '1' ? true : false
    };

    return data;
  }

  onSetGroupName(groupName: string) {
    this.employeeForms.patchValue({
      groupName: groupName,
    });
  }

  onChangeStatus(data: MatRadioChange) {
    this.selectedValue = data.value;
  }

  onClickCancel() {
    this._employeeService.onNavigatePage('/admin/employee');
  }

  getLastId(): number {
    const currentData = this._employeeService.getEmployees();
    const employeeIds = currentData.map((employee) => employee.employeeId);
    return Math.max(...employeeIds) + 1;
  }

  getTitle() {
    return this.employeeId !== 0 ? 'Update Employee' : 'Create Emplyee';
  }

  getAction() {
    return this.employeeId !== 0 ? 'Update' : 'Create';
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
    this._stateService.setEmployeeId(0);
  }
}
