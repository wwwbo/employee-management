import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'currency',
})
export class CurrencyPipe implements PipeTransform {
  transform(value: number, ...args: any[]): string {
    if (value == null) {
      return '';
    }

    const formattedValue = value.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');

    const replacedValue = formattedValue.replace('.', ',');

    return `Rp. ${replacedValue}`;
  }
}
