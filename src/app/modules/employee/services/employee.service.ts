import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Employee, Employees } from '../models/employee.model';
import { BehaviorSubject, Observable, firstValueFrom, map, tap } from 'rxjs';
import { Response } from 'src/app/core/models/app.model';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { SharedService } from 'src/app/shared/services/shared.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class EmployeeService {
  private _currentEmployeeData$ = new BehaviorSubject<Employees>([]);
  private _searchKey$ = new BehaviorSubject<string>('');

  constructor(
    private _http: HttpClient,
    public dialog: MatDialog,
    private _sharedService: SharedService,
    private _router: Router
  ) {}

  get currentEmployeeData$(): Observable<Employees> {
    return this._currentEmployeeData$.asObservable();
  }

  get searchKey$(): Observable<string> {
    return this._searchKey$.asObservable();
  }

  fetchDataEmployee() {
    return this._http.get<Response<Employees>>('assets/data/data.json').pipe(
      map((r: Response<Employees>) => r.data),
      tap((data: Employees) => this.onSetData(data))
    );
  }

  getEmployees() {
    return this._currentEmployeeData$.value;
  }

  getEmployeeId(employeeId: number) {
    return this.getEmployees().filter((d) => d.employeeId === employeeId);
  }

  onSetData(data: Employees) {
    this._currentEmployeeData$.next(data);
  }

  onCreateEmployee(data: Employee) {
    const currentData = this._currentEmployeeData$.value;
    this._currentEmployeeData$.next([...currentData, data]);
    this._sharedService.onShowSnackBar('Employee created');
  }

  onUpdateEmployee(data: Employee) {
    const currentData = this._currentEmployeeData$.value;
    currentData.map(d => {
      if (d.employeeId === data.employeeId) {
        d.firstName = data.firstName,
        d.lastName = data.lastName,
        d.email = data.email,
        d.birthDate = data.birthDate,
        d.basicSalary = data.basicSalary,
        d.group = data.group,
        d.status = data.status
      }
      return d;
    });

    this._currentEmployeeData$.next(currentData);
    this._sharedService.onShowSnackBar('Successfully update data');
  }

  async onDeleteEmployee(employeeId: number) {
    const dialog = await firstValueFrom(
      this.dialog.open(ConfirmDialogComponent).afterClosed()
    );

    if (dialog) {
      const currentData = this._currentEmployeeData$.value;
      const data = currentData.filter((d) => d.employeeId !== employeeId);
      this.onSetData(data);
      this._sharedService.onShowSnackBar('Successfully delete data', true);
    }
  }

  onNavigatePage(path: string) {
    this._router.navigate([path]);
  }

  onSetValueSearch(val: string) {
    this._searchKey$.next(val);
  }
}
