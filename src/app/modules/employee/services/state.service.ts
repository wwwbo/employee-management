import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class StateService {
  private _employeeId$ = new BehaviorSubject<number>(0);

  constructor() {}

  setEmployeeId(employeeId: number) {
    this._employeeId$.next(employeeId);
  }

  getEmployeeId() {
    return this._employeeId$.value;
  }
}
