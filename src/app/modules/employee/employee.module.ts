import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeeRoutingModule } from './employee-routing.module';
import { EmployeeIndexComponent } from './pages/employee-index/employee-index.component';
import { FormsComponent } from './pages/forms/forms.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EmployeeDetailComponent } from './pages/employee-detail/employee-detail.component';
import { DropdownSearchComponent } from './components/dropdown-search/dropdown-search.component';
import { ClickOutsideDirective } from './directives/clickoutside.directive';
import { CurrencyPipe } from './pipes/currency.pipe';


@NgModule({
  declarations: [
    EmployeeIndexComponent,
    FormsComponent,
    EmployeeDetailComponent,
    DropdownSearchComponent,
    ClickOutsideDirective,
    CurrencyPipe
  ],
  imports: [
    CommonModule,
    EmployeeRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class EmployeeModule { }
