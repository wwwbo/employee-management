export interface Employee {
  employeeId: number;
  userName?: string;
  firstName: string;
  lastName: string;
  email: string;
  birthDate: string;
  basicSalary: number;
  status: boolean;
  group: string;
  description?: string;
}

export type Employees = Employee[];

export interface Group {
  groupId: number;
  groupName: string;
}

export type Groups = Group[];
