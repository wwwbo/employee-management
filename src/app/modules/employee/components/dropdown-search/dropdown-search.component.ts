import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Group, Groups } from '../../models/employee.model';
import { AppGroups } from '../../constants/app.employee';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-dropdown-search',
  templateUrl: './dropdown-search.component.html',
  styleUrls: ['./dropdown-search.component.scss'],
})
export class DropdownSearchComponent implements OnInit {
  @Output() emitGroupName: EventEmitter<string> = new EventEmitter<string>();
  isActive: boolean = false;
  groups: Groups | undefined;
  filteredGroups: Groups | undefined;
  groupName: string = '';
  forms!: FormGroup;

  constructor(private _fb: FormBuilder) {}

  ngOnInit(): void {
    this.groups = AppGroups;
    this.filteredGroups = this.groups;
    this.initForm();
  }

  initForm() {
    this.forms = this._fb.group({
      groupName: ['', [Validators.required]]
    })
  }

  onClickInput() {
    this.isActive = true;
  }

  onClickOut() {
    this.isActive = false;
  }

  onKeyPress() {}

  onClickGroup(data: Group) {
    this.groupName = data.groupName;
    this.emitGroupName.emit(data.groupName);
  }

  onFilterData() {
    if (!this.groups) {
      this.filteredGroups = this.groups;
    } else {
      this.filteredGroups = this.groups.filter((group) =>
        group.groupName.toLowerCase().includes(this.groupName.toLowerCase())
      );
    }
  }
}
