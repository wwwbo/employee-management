import { Groups } from '../models/employee.model';

export const AppGroups: Groups = [
  { groupId: 1, groupName: 'Human Resource' },
  { groupId: 2, groupName: 'Product Support' },
  { groupId: 3, groupName: 'Product Design' },
  { groupId: 4, groupName: 'Product Owner' },
  { groupId: 5, groupName: 'Software Engineer' },
  { groupId: 6, groupName: 'People Learning' },
  { groupId: 7, groupName: 'People Service' },
  { groupId: 8, groupName: 'System Analyst' },
  { groupId: 9, groupName: 'Digital Marketing' },
  { groupId: 10, groupName: 'Data Engineer' },
];
