import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmployeeIndexComponent } from './pages/employee-index/employee-index.component';
import { FormsComponent } from './pages/forms/forms.component';
import { EmployeeDetailComponent } from './pages/employee-detail/employee-detail.component';

const routes: Routes = [
  {
    path: '',
    component: EmployeeIndexComponent,
  },
  {
    path: 'create',
    component: FormsComponent,
  },
  {
    path: 'update',
    component: FormsComponent,
  },
  {
    path: 'detail',
    component: EmployeeDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmployeeRoutingModule {}
