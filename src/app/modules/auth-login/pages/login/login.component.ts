import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { User } from '../../models/auth.model';
import { AppUser } from '../../constants/app-user';
import { SharedService } from 'src/app/shared/services/shared.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
  onDestroy$ = new Subject<void>();
  loginForm!: FormGroup;
  appUser: User | undefined;

  constructor(private _fb: FormBuilder, private _router: Router, private _sharedService: SharedService) {}

  ngOnInit(): void {
    this.initData();
    this.initForms();
  }

  initData() {
    this.appUser = AppUser;
  }

  initForms() {
    this.loginForm = this._fb.group({
      userName: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  onClickLogin() {
    if (!this.loginForm.valid) this._sharedService.onShowSnackBar('Username or Password cannot be blank', true);
    const userNameVal = this.loginForm.get('userName')?.value;
    const passwordVal = this.loginForm.get('password')?.value;
    if (
      this.appUser?.userName === userNameVal &&
      this.appUser?.password === passwordVal
    ) {
      this._router.navigate(['/admin']);
    } else {
      this._sharedService.onShowSnackBar('Username or Password incorect', true)
    }
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
